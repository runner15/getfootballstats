import requests, json, sqlite3, os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "Baseball")
#conn = sqlite3.connect(db_path)
#c = conn.cursor()

import plotly.plotly as py
from plotly.graph_objs import *

trace0 = Scatter(
                 x=[1, 2, 3, 4],
                 y=[10, 15, 13, 17]
                 )
trace1 = Scatter(
                 x=[1, 2, 3, 4],
                 y=[16, 5, 11, 9]
                 )
data = Data([trace0, trace1])

py.plot(data, filename = 'basic-line')
