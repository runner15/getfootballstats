import requests, json, sqlite3, os
from variables import insertTeamFun, insertPlayerMFLFun, insertPlayerMSFFun
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "PlayerStats")
conn = sqlite3.connect(db_path)
c = conn.cursor()
#playersMFL = requests.get('https://www77.myfantasyleague.com/2018/export?TYPE=players&DETAILS=1&SINCE=&PLAYERS=&JSON=1')
#playersMFL = playersMFL.json()
#for player in playersMFL['players']['player']:
#    insertPlayerMFLFun(player,c)
#
#teamStatsMSF = requests.get('https://api.mysportsfeeds.com/v1.2/pull/nfl/2017-2018-regular/overall_team_standings.json', auth=('runner15', 'NewPass4321'))
#teamStatsMSF = teamStatsMSF.json()
#for team in teamStatsMSF['overallteamstandings']['teamstandingsentry']:
#    insertTeamFun(team,c)
#
#playerStatsMSF = requests.get('https://api.mysportsfeeds.com/v1.2/pull/nfl/2016-2017-regular/daily_player_stats.json?fordate=20170101', auth=('runner15', 'NewPass4321'))
#playerStatsMSF = playerStatsMSF.json()
#
#for key in playerStatsMSF['dailyplayerstats']['playerstatsentry'][0]['stats'].keys():
#    print(key)
#
playersMSF = requests.get('https://api.mysportsfeeds.com/v1.2/pull/nfl/2017-2018-regular/active_players.json', auth=('runner15', 'NewPass4321'))
playersMSF = playersMSF.json()
for player in playersMSF['activeplayers']['playerentry']:
    insertPlayerMSFFun(player,c)


conn.commit() # Save (commit) the changes
conn.close()# Close the connection
print('Done')
