import requests, json, sqlite3, os
from variablesBaseball import insertPlayer15,insertPlayer18,insertStats
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "Baseball")
conn = sqlite3.connect(db_path)
c = conn.cursor()

players15 = requests.get('http://lookup-service-prod.mlb.com/json/named.roster_team_alltime.bam?start_season=2015&end_season=2015&team_id=111')
players15 = players15.json()
year = '2015'
for player in players15['roster_team_alltime']['queryResults']['row']:
    insertPlayer15(player,c)
    playerStats = requests.get('http://lookup-service-prod.mlb.com/json/named.sport_hitting_tm.bam?league_list_id=\'mlb\'&game_type=\'R\'&season=\''+year+'\'&player_id=\''+player.get('player_id')+'\'')
    playerStats = playerStats.json()
    if(playerStats['sport_hitting_tm']['queryResults'].get('row')):
#        print(type(playerStats['sport_hitting_tm']['queryResults']['row']))
        if(type(playerStats['sport_hitting_tm']['queryResults']['row'])==list):
            for stat in playerStats['sport_hitting_tm']['queryResults']['row']:
                if(stat.get('team_abbrev')=="Bos"):
                    insertStats(stat,c,year)
        else:
            insertStats(playerStats['sport_hitting_tm']['queryResults']['row'],c,year)

players18 = requests.get('http://lookup-service-prod.mlb.com/json/named.roster_40.bam?team_id=111')
players18 = players18.json()
year = '2018'
for player in players18['roster_40']['queryResults']['row']:
    insertPlayer18(player,c)
    playerStats = requests.get('http://lookup-service-prod.mlb.com/json/named.sport_hitting_tm.bam?league_list_id=\'mlb\'&game_type=\'R\'&season=\''+year+'\'&player_id=\''+player.get('player_id')+'\'')
    playerStats = playerStats.json()
    if(playerStats['sport_hitting_tm']['queryResults'].get('row')):
        insertStats(playerStats['sport_hitting_tm']['queryResults']['row'],c,year)


conn.commit() # Save (commit) the changes
conn.close()# Close the connection
print('Done')
