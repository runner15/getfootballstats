def insertPlayer15(player,c):
    insertPlayer = [player.get('name_first_last'),player.get('weight'),player.get('primary_position'),player.get('birth_date'),player.get('throws'),player.get('stat_years'),player.get('height_inches'),player.get('name_sort'),player.get('status_short'),player.get('jersey_number'),player.get('player_first_last_html'),player.get('bats'),player.get('position_desig'),player.get('forty_man_sw'),player.get('player_html'),player.get('height_feet'),player.get('player_id'),player.get('name_last_first'),player.get('current_sw'),player.get('team_id'),player.get('roster_years'),player.get('active_sw')]
    c.execute('INSERT or REPLACE INTO redsoxPlayers2015 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',insertPlayer)
    return

def insertPlayer18(player,c):
    insertPlayer = [player.get('position_txt'),player.get('weight'),player.get('name_display_first_last'),player.get('college'),player.get('height_inches'),player.get('starter_sw'),player.get('jersey_number'),player.get('end_date'),player.get('name_first'),player.get('bats'),player.get('team_code'),player.get('height_feet'),player.get('pro_debut_date'),player.get('status_code'),player.get('primary_position'),player.get('birth_date'),player.get('team_abbrev'),player.get('throws'),player.get('team_name'),player.get('name_display_last_first'),player.get('name_use'),player.get('player_id'),player.get('name_last'),player.get('team_id'),player.get('start_date'),player.get('name_full')]
    c.execute('INSERT or REPLACE INTO redsoxPlayers2018 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',insertPlayer)
    return

def insertStats(stat,c,year):
#    print(type(stat))
    insertStat = [stat.get('gidp'),stat.get('sac'),stat.get('np'),stat.get('sport_code'),stat.get('hgnd'),stat.get('tb'),stat.get('gidp_opp'),stat.get('sport_id'),stat.get('bb'),stat.get('avg'),stat.get('slg'),stat.get('team_full'),stat.get('ops'),stat.get('hbp'),stat.get('league_full'),stat.get('team_abbrev'),stat.get('so'),stat.get('hfly'),stat.get('wo'),stat.get('league_id'),stat.get('sf'),stat.get('team_seq'),stat.get('league'),stat.get('hpop'),stat.get('cs'),stat.get('season'),stat.get('sb'),stat.get('go_ao'),stat.get('ppa'),stat.get('player_id'),stat.get('ibb'),stat.get('team_id'),stat.get('roe'),stat.get('go'),stat.get('hr'),stat.get('rbi'),stat.get('babip'),stat.get('lob'),stat.get('end_date'),stat.get('xbh'),stat.get('league_short'),stat.get('g'),stat.get('d'),stat.get('sport'),stat.get('team_short'),stat.get('tpa'),stat.get('h'),stat.get('obp'),stat.get('hldr'),stat.get('t'),stat.get('ao'),stat.get('r'),stat.get('ab'),year,stat.get('player_id')+year]
    c.execute('INSERT or REPLACE INTO playerStats VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',insertStat)
    return
